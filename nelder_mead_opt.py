#--------------------------------------------------------------
#
# Source code of Collie River Basin 2 hydrological model
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
import scipy.optimize
import numpy as np
import matplotlib.pyplot as plt
import yaml

def NSE(f,o): #Nash-Sutcliffe Model Efficiency Coefficient
    clim = np.ones((len(o)))*np.nanmean(o)
    numerator = np.nansum((f-o)**2)
    denominator = np.nansum((o-clim)**2)
    return 1 - (numerator/denominator)

def CRB3(plot_export):
    # reading configuration file
    filename = 'config.yml'
    with open(filename,'r') as ymlfile:
        params = yaml.load(ymlfile)

    # parameters
    Smax = params['parameters']['Smax']
    Sfc = params['parameters']['Sfc'] * Smax
    a = params['parameters']['a']
    M = params['parameters']['M']
    b = params['parameters']['b']
    lam = params['parameters']['lambda']

    # other configulations
    Tend = params['TimePeriod']['Tend']

    # Initializing state variables
    S = np.zeros((Tend+1)) #Storage [mm]
    S[0] = params['InitialConditions']['Storage']
    G = np.zeros(Tend + 1)
    G[0] = params['InitialConditions']['Groundwater']

    # Input meteorological forcings
    filename = params['MERV-JpDATA']['infile']
    data = np.loadtxt(filename,delimiter=',',skiprows=1)

    P = data[0:4018,4] #precipitation [mm/d]
    Ep = data[0:4018,6] #potential evapotranspiration [mm/d]

    # output
    Q = np.zeros((Tend))

    # initializing main exacutable code for the model
    for t in range(0,Tend):
        # Calculating fluxes
        Eb = Ep[t]*(1-M)*S[t]/Smax # Evaporation from soil
        if S[t] > Sfc:
            Ev = M*Ep[t] # transpiration from vegetation
            Qss = a*(S[t]-Sfc) # Subsurface runoff
        else:
            Ev = M*Ep[t]*S[t]/Sfc # transpiration from vegetation
            Qss = 0.0 # subsurface runoff

        if S[t] > Smax:
            Qse = P[t] # saturation excess overland flow
        else:
            Qse = 0.0 # no saturation excess overland flow
        
        Qss_star = lam * Qss

        Qsg = (a*G[t])**b

        Q[t] = (1-lam) * Qss + Qse + Qsg # total runoff

        # Updating Storage
        S[t+1] = S[t] + P[t] - Eb - Ev - Qse - Qss
        G[t+1] = Qss_star - Qsg + G[t]
        if G[t+1] < 0: 
            G[t+1] = 0
            Qsg = G[t] + Qss_star

        # Monitor
        # print('integrating at time', t)
        # print('rainfall, storage, groundwater, runoff = ', P[t], S[t], G[t], Q[t])
    if(plot_export):
        # writing output Q[t] in txt file
        np.savetxt('output_ch3_1.txt',Q)

    # observed runoff
    obsQ = data[0:4018,7]
    obsQ[obsQ<0] = nan

    if(plot_export):
        fig, ax = plt.subplots(figsize=(20,10))
        ax.plot(obsQ[0:Tend],label='observation')
        ax.plot(Q[0:Tend],label='my simulation')
        #ax.plot(referenceQ[0:Tend],label='reference in MERV-Jp')
        ax.legend()
        fig.savefig("res_ch3_2")

    NSEsim = NSE(Q[0:Tend],obsQ[0:Tend])

    print('NSEs for your simulation and reference in MERV-Jp are ', NSEsim)
    return (NSEsim)

def fun(x):
    filename = 'config.yml'
    with open(filename,'r') as ymlfile:
        params = yaml.load(ymlfile)
    params['parameters']['Smax'] = float(x[0])
    params['parameters']['Sfc'] = float(x[1])
    params['parameters']['a'] = float(x[2])
    params['parameters']['M'] = float(x[3])
    params['parameters']['b'] = float(x[4])
    params['parameters']['lambda'] = float(x[5])
    params['InitialConditions']['Storage'] = float(x[6])
    params['InitialConditions']['Groundwater'] = float(x[7])
    with open(filename, 'w') as ymlfile:
        yaml.dump(params, ymlfile)
    res = CRB3(False)
    return -res
    
res = scipy.optimize.minimize(fun, np.array([100, 0.25, 0.75, 0.2, 0.7, 0.9, 10, 5]), bounds = ((1,2000), (0.05,0.95), (0,1), (0.05,0.95), (0, 1), (0,1), (0, 500), (0, 500)), method = 'Nelder-Mead', options={'maxiter': 100})
print(res)
CRB3(True)

