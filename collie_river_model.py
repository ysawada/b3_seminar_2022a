#--------------------------------------------------------------
#
# Source code of Collie River Basin 2 hydrological model
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import yaml

def NSE(f,o): #Nash-Sutcliffe Model Efficiency Coefficient
    clim = np.ones((len(o)))*np.nanmean(o)
    numerator = np.nansum((f-o)**2)
    denominator = np.nansum((o-clim)**2)
    return 1 - (numerator/denominator)

# model parameters
Smax = 1000 # maximum soil moisture storage [mm] [1-2000]
Sfc = 0.50*Smax # Field capacity [mm] [0.05-0.95]
a = 0.5 # runoff coefficient [d^-1] [0-1]
M = 0.7 # Forest fraction [-] [0.05-0.95]

# Other Configuration
Tend = 3650 # Total timesteps [d]

# Initializing state variables
S = np.zeros((Tend+1)) #Storage [mm]
S[0] = 100.0

print('state variable = ', S)

# reading input variables from MERV-Jp
filename = './varssim086.csv'
data = np.loadtxt(filename,delimiter=',',skiprows=1)
P = data[0:4018,4] #precipitation [mm/d]
Ep = data[0:4018,6] #potential evapotranspiration [mm/d]

# output
Q = np.zeros((Tend))

# initializing main exacutable code for the model
for t in range(0,Tend):
    # Calculating fluxes
    Eb = Ep[t]*(1-M)*S[t]/Smax # Evaporation from soil
    if S[t] > Sfc:
        Ev = M*Ep[t] # transpiration from vegetation
        Qss = a*(S[t]-Sfc) # Subsurface runoff
    else:
        Ev = M*Ep[t]*S[t]/Sfc # transpiration from vegetation
        Qss = 0.0 # subsurface runoff

    if S[t] > Smax:
        Qse = P[t] # saturation excess overland flow
    else:
        Qse = 0.0 # no saturation excess overland flow

    Q[t] = Qss + Qse # total runoff

    # Updating Storage
    S[t+1] = S[t] + P[t] - Eb - Ev - Qse - Qss

    # Monitor
    print('integrating at time', t)
    print('rainfall, storage, runoff = ', P[t], S[t], Q[t])


# observed runoff
obsQ = data[0:4018,7]
obsQ[obsQ<0] = nan


plt.plot(obsQ[0:Tend],label='observation')
plt.plot(Q[0:Tend],label='my simulation')
plt.legend()
plt.show()

NSEsim = NSE(Q[0:Tend],obsQ[0:Tend])

print('NSEs for your simulation and reference in MERV-Jp are ', NSEsim)

